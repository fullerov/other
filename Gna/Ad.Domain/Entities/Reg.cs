﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Security.Principal;
using Ad.Domain.Attributes;
using Ad.Domain.Concrete;
using Ad.Domain.Resources;

namespace Ad.Domain.Entities
{
    [DataContract]
    public class Reg:IEntity
    {

        public int Id { get; set; }

        [DataMember]
        public int RealId { get; set; }

        [DataMember]
        public int CodeId { get; set; }


        [DataMember]
        public string Code { get; set; }

        [DataMember]
        public string Fio { get; set; }

         [DataMember]
        public string Birthdate { get; set; }

        [DataMember]
         public string Phone { get; set; }

        [DataMember]
        public string AddPhone { get; set; }

        [DataMember]
        public string Loyal { get; set; }

        [DataMember]
        public string Date{ get; set; }

       [DataMember]
        public string Ip{ get; set; }
    }
}