﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Ad.Domain.Entities
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
