﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Core.Objects;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Ad.Domain.Abstract;
using Ad.Domain.Entities;

namespace Ad.Domain.Concrete
{
    public class EFRepository : IRepository
    {
        private readonly EFDbContext _dbContext = new EFDbContext();

        #region Базовые операции

        /// <summary>
        /// Использовать если экземпляр Entity получен из другого контекста
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        public void Attach<T>(T entity) where T : class
        {
            _dbContext.Set<T>().Attach(entity);
        }


        public IQueryable<T> GetAll<T>() where T : class
        {
            return _dbContext.Set<T>();
        }

        public T SingleOrDefault<T>(Func<T, bool> predicate) where T : class
        {
            return _dbContext.Set<T>().SingleOrDefault(predicate);
        }

        public T FirstOrDefault<T>(Func<T, bool> predicate) where T : class
        {
            return _dbContext.Set<T>().FirstOrDefault(predicate);
        }

        public void Add<T>(T entity) where T : class
        {
            _dbContext.Set<T>().Add(entity);
        }

        public int SaveOrUpdate<T>(T entity)
            where T : class, IEntity
        {
            if (entity.Id == 0)
                _dbContext.Set<T>().Add(entity);
            else
            {
                T dbEntry = _dbContext.Set<T>().Find(entity.Id);
                if (dbEntry != null) dbEntry = entity;
            }

            return _dbContext.SaveChanges();
        }

        public async Task<int> SaveOrUpdateAsync<T>(T entity)
        where T : class, IEntity
        {
            if (entity.Id == 0)
                _dbContext.Set<T>().Add(entity);
            else
            {
                T dbEntry = _dbContext.Set<T>().Find(entity.Id);
                if (dbEntry != null) dbEntry = entity;
            }

            return await _dbContext.SaveChangesAsync();
        }

        public void Remove<T>(T entity) where T : class
        {
            _dbContext.Set<T>().Remove(entity);
        }

        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            await _dbContext.SaveChangesAsync();
        }
        public void Dispose()
        {
            _dbContext.Dispose();
        }

        #endregion

    }
}