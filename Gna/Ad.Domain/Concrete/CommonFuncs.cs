﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Device.Location;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using Ad.Domain.Entities;
using Newtonsoft.Json;

namespace Ad.Domain.Concrete
{
    public static class CommonFuncs
    {
        public static double GetDistance(double latP1, double longP1, double latP2, double longP2)
        {
            var sCoord = new GeoCoordinate(latP1, longP1);
            var eCoord = new GeoCoordinate(latP2, longP2);
            //var g = new System.Device.Location.CivicAddressResolver();
            //var info = g.ResolveAddress(sCoord);
            return sCoord.GetDistanceTo(eCoord);
        }

    

        public static List<String> GetTelsWithCorrectFormat(List<String> elements)
        {
            var tels = new List<String>();
            foreach (var e in elements)
            {
                var w = String.Join("", e.Where(Char.IsDigit));
                if (w.First() == '0')
                {
                    w = w.Substring(1, w.Length - 1);
                    //todo Подставлять нужный код страны, взависимости от выбранной страны в фильтре
                    w = @"996" + w;
                }
                if (w.Length >= 9 && tels.All(a => a != w))
                    tels.Add(w);
            }
            return tels;
        }
    }
}