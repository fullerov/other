﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Ad.Domain.Concrete
{
    public interface IKeyProvider
    {
        byte[] getKey { get; }
    }

    public class KeyProvider : IKeyProvider
    {
        public byte[] getKey
        {
            get { return Convert.FromBase64String(Properties.Resources.ResourceManager.GetString("Key")); }
        }
    }

    public class Crypto
    {
        enum CryptDirection
        {
            Encrypt,
            Decrypt
        }
        public static string EncriptMessage(IKeyProvider keyProvider, string message)
        {
            var input = Encoding.UTF32.GetBytes(message);
            RSACryptoServiceProvider key = new RSACryptoServiceProvider();
            key.ImportCspBlob(keyProvider.getKey);
            var res = cryptMessageInner(input, key, CryptDirection.Encrypt);
            return res;
        }
        public static string DecriptMessage(IKeyProvider keyProvider, string message)
        {
            RSACryptoServiceProvider key = new RSACryptoServiceProvider();
            key.ImportCspBlob(keyProvider.getKey);
            var input = message.ToCharArray().Select(a => (byte)a).ToArray();
            var res = cryptMessageInner(input, key, CryptDirection.Decrypt);
            return res;
        }

        private static string cryptMessageInner(byte[] message, RSACryptoServiceProvider key, CryptDirection direction)
        {
            if (message.Length == 0)
                return string.Empty;
            int maxLen = direction == CryptDirection.Encrypt ? 128 : 256;
            byte[] forCrypt = message;
            if (message.Length > maxLen)
            {
                forCrypt = message.Take(maxLen).ToArray();
                message = message.Skip(maxLen).ToArray();
            }
            else
                message = new byte[0];
            var tmp = direction == CryptDirection.Encrypt ? key.Encrypt(forCrypt, false) : key.Decrypt(forCrypt, false);
            var charArray = tmp.Select(a => (char)a);
            var res = direction == CryptDirection.Encrypt ? new string(charArray.ToArray()) : Encoding.UTF32.GetString(tmp);
            res += cryptMessageInner(message, key, direction);
            return res;
        }
        static public string MD5_old(string s)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(s);
            MD5CryptoServiceProvider csp = new MD5CryptoServiceProvider();
            byte[] bytehash = csp.ComputeHash(bytes);
            string hash = string.Empty;
            foreach (byte b in bytehash)
                //hash += string.Format("{0:x2}", b);
                hash += b;
            return hash;
        }


        public static string MD5_hash(string password)
        {
            byte[] textBytes = Encoding.Default.GetBytes(password);
            try
            {
                MD5CryptoServiceProvider cryptHandler = new MD5CryptoServiceProvider();
                byte[] hash = cryptHandler.ComputeHash(textBytes);
                string ret = "";
                foreach (byte a in hash)
                {
                    ret += a.ToString("x2");
                }
                return ret;
            }
            catch
            {
                throw;
            }
        }
    }
}
