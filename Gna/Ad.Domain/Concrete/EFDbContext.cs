﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Ad.Domain.Entities;
using Ad.Domain.Migrations;

namespace Ad.Domain.Concrete
{
    public class EFDbContext : DbContext
    {
        public EFDbContext()
        {
            // установка инициализации бд - стратегии миграции IDatabaseInitializer
            Database.SetInitializer<EFDbContext>(new MigrateDatabaseToLatestVersion<EFDbContext, Configuration>());
            //Configuration.ProxyCreationEnabled = false;
            Database.CommandTimeout = 120;
        }

        public DbSet<Reg> Reg { get; set; }


    }
}