﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Security.Policy;
using System.Web;
using Ad.Domain.Abstract;
using Ad.Domain.Concrete;
using Ad.Domain.Entities;

namespace Ad.Domain.Concrete
{
    public static class Global
    {
        public const string UserSession = "UserSessionState";
        public const string CityId = "CityId";
        public const string RegionId = "RegionId";
        public const string CountryId = "CountryId";
        public const string TitlePrefix = " / Объявления - SaleToday";

        public static SessionState SessionState
        {
            get
            {
                if (System.Web.HttpContext.Current.Session[UserSession] == null)
                {
                    System.Web.HttpContext.Current.Session[UserSession] = new SessionState();
                }
                return (SessionState)System.Web.HttpContext.Current.Session[UserSession];
            }
        }
    }
    public class SessionState
    {
        private EFRepository _rep
        {
            get { return new EFRepository(); }
        }

              private string _sessionID;

        public string SessionID
        {
            get
            {
                if (_sessionID == null)
                    _sessionID = HttpContext.Current.Session.SessionID;
                return _sessionID;
            }
        }
        public string GetClienIp
        {
            get
            {
                // Look for a proxy address first
                var ip = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                // If there is no proxy, get the standard remote address
                if (ip == null || ip.ToLower() == "unknown")
                    ip = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                return ip;
            }
        }

  

     

        public SessionState()
        {
            _sessionID = HttpContext.Current.Session.SessionID;
        }

        public void NewSessionState()
        {
            HttpContext.Current.Session["UserSessionState"] = new SessionState();
        }

     
    }
}