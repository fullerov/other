﻿using System;
using System.Net;
using System.Net.Sockets;
using Ad.Domain.Concrete;
using Ad.Domain.Entities;

namespace Ad.Domain.Concrete
{
    public class MyTcpClient
    {
        public TcpClient TcpClient { get; set; }
    }

    
    public delegate void LogEventHandler(LogEventArgs e);

    public delegate void ProcessEventHandler(LogEventArgs e);

    public class ProcessEventArgs : EventArgs
    {
        private readonly string _message;

        public ProcessEventArgs(string message)
        {
            this._message = message;
        }

        public string Message
        {
            get { return _message; }
        }
    }
    
    /// <summary>
    /// Summary description for LogEventArgs.
    /// </summary>
    public class LogEventArgs : EventArgs
    {
        private readonly string _message;
        private readonly MyTcpClient _client;

        public LogEventArgs(string message, MyTcpClient client)
        {
            this._message = message + "";
            this._client = client;
        }//LogEventArgs

        public string Message
        {
            get { return _message; }
        }//Message

        public MyTcpClient MyTcpClient
        {
            get { return _client; }
        }

    }//LogEventArgs
}