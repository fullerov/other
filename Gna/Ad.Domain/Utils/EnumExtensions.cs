﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Ad.Domain.Utils
{
    public static class EnumExtensions
    {
        public static string DisplayName(this Enum enumVal)
        {
            var type = enumVal.GetType();
            var memInfo = type.GetMember(enumVal.ToString());
            var attribute = memInfo[0].GetCustomAttributes(typeof(DisplayAttribute), false).FirstOrDefault();
            if (attribute == null)
                return enumVal.ToString();
            return ((DisplayAttribute)attribute).GetName();
        } 
    }
}