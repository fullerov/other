﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ad.Domain.Entities;

namespace Ad.Domain.Abstract
{
    public interface IRepository : IDisposable
    {
        IQueryable<T> GetAll<T>() where T : class;
        T SingleOrDefault<T>(Func<T, bool> predicate) where T : class;
        T FirstOrDefault<T>(Func<T, bool> predicate) where T : class;
        void Add<T>(T entity) where T : class;
        void Remove<T>(T entity) where T : class;
        void SaveChanges();

 
    }
}