﻿using Ad.Domain.Concrete;
using Ad.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Gna
{
    // ПРИМЕЧАНИЕ. Команду "Переименовать" в меню "Рефакторинг" можно использовать для одновременного изменения имени класса "Service1" в коде, SVC-файле и файле конфигурации.
    // ПРИМЕЧАНИЕ. Чтобы запустить клиент проверки WCF для тестирования службы, выберите элементы Service1.svc или Service1.svc.cs в обозревателе решений и начните отладку.
    public class Service1 : IService1
    {
        public bool Status()
        {
            return true;
        }

         public void Register(Reg reg)
        {

            var rep = new EFRepository();
           
                var newreg = new Reg();
                newreg.RealId = reg.RealId;
                newreg.AddPhone = reg.AddPhone;
                newreg.Birthdate = reg.Birthdate;
                newreg.Code = reg.Code;
                newreg.CodeId = reg.CodeId;
                newreg.Date = reg.Date;
                newreg.Fio = reg.Fio;
                newreg.Ip = reg.Ip;
                newreg.Loyal = reg.Loyal;
                newreg.Phone = reg.Phone;

            rep.Add(newreg);
            rep.SaveChanges();

           
        }

    }

}

